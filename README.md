# StarWarsTestAPI
##Arrk SWARS iOS Challenge

Apologies, I never finished the task in the time limit. I spent too much time researching about SOLID principles and rediscovering Generics. 

I have tried to go for the classic approach with using an APIManager which is a singleton. The apimanager is injected to make the app more testable. I have refrained from using any pods or frameworks and used Apple’s built in JSON decoders/encoders and their networking framework. 

With more time i would have liked to make the APIManager more abstract and create a higher level class which calls upon it. I would possibly look into turning my Models into Classes so they can inheit a common class which would make it easier to return specific types. Apologies for the code not being too clean. i think the 1-2 hours wasnt enough for me !

## ToDo
- Unit tests
- Search
- Pagination results (get more results)
- SOLID Principles
- Accessibility Features 
- Landscape mode
- Animated Launch screen


