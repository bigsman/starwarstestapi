//
//  ResourceDetailViewController.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

class ResourceDetailViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	
	var apiManager: APIManager!

	var resource: Resource!
	
	var items = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()

		getItems()
    }

	func getItems() {
		
//		apiManager.loadSourceDetails(for: resource) { (result, error) in
//			print(result)
//		}

		
		switch resource.name {
		case "films":
			apiManager.loadFilmResource(resource: resource) { (result, error) in
				guard error == nil else {
					print("Error with the request")
					return
				}
				if let result = result {
					
				}
			}
		case "people":
			apiManager.loadResourceDetail(url: resource.url, resourceType: PeopleResult.self) { (result, error) in
				guard error == nil else {
					print("Error with the request")
					return
				}
			}
		case "planets":
			apiManager.loadResourceDetail(url: resource.url, resourceType: PlanetResult.self) { (result, error) in
				guard error == nil else {
					print("Error with the request")
					return
				}
			}
		case "species":
			apiManager.loadResourceDetail(url: resource.url, resourceType: SpeciesResult.self) { (result, error) in
				guard error == nil else {
					print("Error with the request")
					return
				}
			}
		case "starships":
			apiManager.loadResourceDetail(url: resource.url, resourceType: StarshipsResult.self) { (result, error) in
				guard error == nil else {
					print("Error with the request")
					return
				}
			}
		case "vehicles":
			apiManager.loadResourceDetail(url: resource.url, resourceType: VehicleResult.self) { (result, error) in
				guard error == nil else {
					print("Error with the request")
					return
				}
			}
		default: ()
		
		}

		//apiManager.loadResourceDetail(url: resource.url, resourceType: FilmResult.self)
		
//		apiManager.loadResourceDetail(resource.url, resourceType: Film.self) { (result, error) in
//			guard error == nil else {
//				print("Error with the request")
//				return
//			}
//
//			if let resources = result {
//				//self.items = resources
//			}
//
//			OperationQueue.main.addOperation {
//				self.tableView.reloadData()
//			}
//		}

	}

}

extension ResourceDetailViewController: UITableViewDataSource, UITableViewDelegate {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ResourceDetailCellID", for: indexPath)

		//let resource = items[indexPath.row]

		//cell.textLabel?.text = resource.name

		return cell
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if (indexPath.row % 2 == 0) {
			cell.backgroundColor = UIColor.lightText
		} else {
			cell.backgroundColor = UIColor.white
		}
	}

}
