//
//  ViewController.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

enum State {
	case loading
	case populated([Resource])
	case empty
	case error(Error)
}

class ResourceViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	
	var apiManager: APIManager!
	
	var state = State.loading
	var items = [Resource]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.title = "StarWars API"

		apiManager.loadResources() { (result, error) in
			guard error == nil else {
				self.state = State.error(error!)
				print("Error with the request")
				return
			}
			
			if let resources = result {
				self.state = State.populated(resources)
				self.items = resources
			}
			
			OperationQueue.main.addOperation {
				self.tableView.reloadData()
			}
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == "ShowResourceDetail") {
			let vc = segue.destination as! ResourceDetailViewController
			
			if let row = tableView.indexPathForSelectedRow?.row {
				let selectedItem = items[row]
				vc.resource = selectedItem
				vc.title = selectedItem.name.capitalized
				vc.apiManager = apiManager
			}
		}
	}
}

extension ResourceViewController: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ResourceCellID", for: indexPath)
	
		let resource = items[indexPath.row]

		cell.textLabel?.text = resource.name.capitalized
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)
	}
}

