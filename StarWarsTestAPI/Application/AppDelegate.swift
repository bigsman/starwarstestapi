//
//  AppDelegate.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		application.statusBarStyle = .lightContent

		
		let apiManager = APIManager.sharedInstance

		let navController = window!.rootViewController as! UINavigationController
		let resourceViewController = navController.topViewController as! ResourceViewController
		resourceViewController.apiManager = apiManager
		
		return true
	}
}

