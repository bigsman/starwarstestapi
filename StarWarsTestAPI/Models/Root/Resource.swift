//
//  Resource.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

struct Resource: Codable {	
	let name: String
	let url: String
}
