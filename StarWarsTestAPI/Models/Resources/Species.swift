//
//  Species.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

struct SpeciesResult: Codable {
	let count: Int
	let next, previous: URL?
	let results: [Species]
}

struct Species: Codable {
	let name, classification, designation: String
	let averageHeight, skinColors, hairColors, eyeColors: String
	let averageLifespan: String
	let homeworld: String
	let language: String
	let people, films: [String]
	let created, edited: String
	let url: String
	
	enum CodingKeys: String, CodingKey {
		case name, classification, designation
		case averageHeight = "average_height"
		case skinColors = "skin_colors"
		case hairColors = "hair_colors"
		case eyeColors = "eye_colors"
		case averageLifespan = "average_lifespan"
		case homeworld, language, people, films, created, edited, url
	}
}
