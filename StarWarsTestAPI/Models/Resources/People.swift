//
//  People.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

struct PeopleResult: Codable {
	let count: Int
	let next, previous: URL?
	let results: [People]
}

struct People: Codable {
	let name, height, mass, hairColor: String
	let skinColor, eyeColor, birthYear: String
	let gender: Gender
	let homeworld: String
	let films, species, vehicles, starships: [String]
	let created, edited: String
	let url: String
	
	enum CodingKeys: String, CodingKey {
		case name, height, mass
		case hairColor = "hair_color"
		case skinColor = "skin_color"
		case eyeColor = "eye_color"
		case birthYear = "birth_year"
		case gender, homeworld, films, species, vehicles, starships, created, edited, url
	}
}

enum Gender: String, Codable {
	case female = "female"
	case male = "male"
	case nA = "n/a"
}
