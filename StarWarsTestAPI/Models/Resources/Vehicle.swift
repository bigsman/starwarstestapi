//
//  Vehicle.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

struct VehicleResult: Codable {
	let count: Int
	let next, previous: URL?
	let results: [Vehicle]
}

struct Vehicle: Codable {
	let name, model, manufacturer, costInCredits: String
	let length, maxAtmospheringSpeed, crew, passengers: String
	let cargoCapacity, consumables, vehicleClass: String
	let pilots, films: [String]
	let created, edited: String
	let url: String
	
	enum CodingKeys: String, CodingKey {
		case name, model, manufacturer
		case costInCredits = "cost_in_credits"
		case length
		case maxAtmospheringSpeed = "max_atmosphering_speed"
		case crew, passengers
		case cargoCapacity = "cargo_capacity"
		case consumables
		case vehicleClass = "vehicle_class"
		case pilots, films, created, edited, url
	}
}
