//
//  Film.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

struct FilmResult: Codable {
	let count: Int
	let next, previous: URL?
	let results: [Film]
}

struct Film: Codable {
	let title: String
	let episodeID: Int
	let openingCrawl, director, producer, releaseDate: String
	let characters, planets, starships, vehicles: [String]
	let species: [String]
	let created, edited: String
	let url: String
	
	enum CodingKeys: String, CodingKey {
		case title
		case episodeID = "episode_id"
		case openingCrawl = "opening_crawl"
		case director, producer
		case releaseDate = "release_date"
		case characters, planets, starships, vehicles, species, created, edited, url
	}
}
