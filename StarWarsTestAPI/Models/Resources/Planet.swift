//
//  Planet.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

struct PlanetResult: Codable {
	let count: Int
	let next, previous: URL?
	let results: [Planet]
}

struct Planet: Codable {
	let name, rotationPeriod, orbitalPeriod, diameter: String
	let climate, gravity, terrain, surfaceWater: String
	let population: String
	let residents, films: [String]
	let created, edited: String
	let url: String
	
	enum CodingKeys: String, CodingKey {
		case name
		case rotationPeriod = "rotation_period"
		case orbitalPeriod = "orbital_period"
		case diameter, climate, gravity, terrain
		case surfaceWater = "surface_water"
		case population, residents, films, created, edited, url
	}
}
