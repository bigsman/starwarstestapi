//
//  APIManager.swift
//  StarWarsTestAPI
//
//  Created by Sohel Seedat on 19/09/2018.
//  Copyright © 2018 Jusho. All rights reserved.
//

import UIKit

enum ResourceType: String {
	case People = "people"
	case Planet = "planets"
	case Film = "films"
	case Species = "species"
	case Vehicle = "vehicles"
	case Starships = "starships"
}

class APIManager {
	
	let baseURL = "https://swapi.co/api/"
	
	static let sharedInstance = APIManager()
	
	func loadResources(_ completion: @escaping (_ allResources: [Resource]?, _ error: Error?) -> ()) {
		self.load(baseURL) { (results, error) in
			completion(results, error)
		}
	}
	
	func load(_ urlString: String, _ completion: @escaping (_ results: [Resource]?, _ error: Error?) -> ()) {
		let url = URL(string: urlString)
		let request = URLRequest(url: url!)
		
		let config = URLSessionConfiguration.default
		let session = URLSession(configuration: config)
		
		let task = session.dataTask(with: request) { (data, response, error) in
			if let jsonData = data {
				do {
					let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: String]
					var items = [Resource]()
					for (key, value) in jsonObject {
						let newResource = Resource(name: key, url: value)
						items.append(newResource)
					}
					completion(items, nil)
				} catch let error {
					completion(nil, error)
				}
			} else if let requestError = error {
				completion(nil, requestError)
			} else {
				print("Unexpected error with the request")
			}
		}
		task.resume()
	}
	
	func decode<T: Decodable>(modelType: T.Type, data: Data) -> T {
		return try! JSONDecoder().decode(modelType, from: data)
	}
	
	func loadResourceDetail<T: Codable>(url urlString: String, resourceType: T.Type, _ completion: @escaping (_ result: T?, _ error: Error?) -> ()) {
		let url = URL(string: urlString)
		let request = URLRequest(url: url!)

		let config = URLSessionConfiguration.default
		let session = URLSession(configuration: config)

		let task = session.dataTask(with: request) { (data, response, error) in
			if let jsonData = data {
				
				let items = self.decode(modelType: resourceType, data: jsonData)
				print(type(of: items))
				print(items)
				completion(items, nil)
			} else if let requestError = error {
				print("Error fetching interesting photos: \(requestError)")
			} else {
				print("Unexpected error with the request")
			}
		}
		task.resume()
	}
	
	
	func loadFilmResource(resource: Resource, _ completion: @escaping (_ result: FilmResult?, _ error: Error?) -> ()) {
		loadResourceDetail(url: resource.name, resourceType: FilmResult.self) { (result, error) in
			completion(result, error)
		}
	}
	
	func loadPeopleResource(resource: Resource, _ completion: @escaping (_ result: PeopleResult?, _ error: Error?) -> ()) {
		loadResourceDetail(url: resource.name, resourceType: PeopleResult.self) { (result, error) in
			completion(result, error)
		}
	}
	
	func loadPlanetResource(resource: Resource, _ completion: @escaping (_ result: PlanetResult?, _ error: Error?) -> ()) {
		loadResourceDetail(url: resource.name, resourceType: PlanetResult.self) { (result, error) in
			completion(result, error)
		}
	}
	
	func loadSpeciesResource(resource: Resource, _ completion: @escaping (_ result: SpeciesResult?, _ error: Error?) -> ()) {
		loadResourceDetail(url: resource.name, resourceType: SpeciesResult.self) { (result, error) in
			completion(result, error)
		}
	}
	
	func loadStarshipsResource(resource: Resource, _ completion: @escaping (_ result: StarshipsResult?, _ error: Error?) -> ()) {
		loadResourceDetail(url: resource.name, resourceType: StarshipsResult.self) { (result, error) in
			completion(result, error)
		}
	}
	
	func loadVehicleResource(resource: Resource, _ completion: @escaping (_ result: VehicleResult?, _ error: Error?) -> ()) {
		loadResourceDetail(url: resource.name, resourceType: VehicleResult.self) { (result, error) in
			completion(result, error)
		}
	}

	
//	func loadSourceDetails<T>(for resource: Resource, _ completion: @escaping (_ result: T?, _ error: Error?) -> ()) {
//		print(resource.name)
//		switch resource.name {
//		case "films":
//			loadResourceDetail(url: resource.url, resourceType: FilmResult.self) { (result, error) in
//				completion(result as FilmResult, error)
//			}
//		case "people":
//			loadResourceDetail(url: resource.url, resourceType: PeopleResult.self) { (result, error) in
//				completion(result as! PeopleResult.self, error)
//			}
//		case "planets":
//			loadResourceDetail(url: resource.url, resourceType: PlanetResult.self) { (result, error) in
//				completion(result as AnyObject, error)
//			}
//		case "species":
//			loadResourceDetail(url: resource.url, resourceType: SpeciesResult.self) { (result, error) in
//				completion(result as AnyObject, error)
//			}
//		case "starships":
//			loadResourceDetail(url: resource.url, resourceType: StarshipsResult.self) { (result, error) in
//				completion(result as AnyObject, error)
//			}
//		case "vehicles":
//			loadResourceDetail(url: resource.url, resourceType: VehicleResult.self) { (result, error) in
//				completion(result as AnyObject, error)
//			}
//		default:
//			loadResourceDetail(url: resource.url, resourceType: FilmResult.self) { (result, error) in
//				completion(result as AnyObject, error)
//			}
//		}
//	}
//
//	func getGeneric<T>(object: T) -> T.Type {
//		return T.self
//	}
	
	

	
	

}
